import React from 'react';
import { StyleSheet, View, Text, Image, ScrollView, Dimensions } from 'react-native'
import { Card, ListItem, Button, Icon, Tile } from 'react-native-elements'



const ListaTexto3 = (props) =>{

    const navegarVideo = () => {
        props.navigation.navigate('ListaTexto4');       
    }


    return (
        <ScrollView>  
            <View style={styles.container}>
                <Tile
                    imageSrc={require('../assets/card1.jpg')}
                    icon={{ name: 'play-circle', type: 'font-awesome' }}
                    featured
                    />      
            </View>
            <View>
                <Card>
                    <Card.Title style={{ backgroundColor: 'orange', color: 'white'}}>TIPS</Card.Title>
                    <Button                        
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor:'darkorange'}}
                        title='VER TIPS'/>                  
                </Card>
            </View>
            <View>
                
                    <Button                        
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor:'darkorange'}}
                        title='VER VIDEO' onPress={()=> navegarVideo()}/>                  
                
            </View>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'col',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
      },
      explicacion: {
        width: '40%' // is 50% of container width
      },
      tips: {
        width: '20%' // is 50% of container width
      },
      video: {
        width: '40%' // is 50% of container width
      }
});
export default ListaTexto3;