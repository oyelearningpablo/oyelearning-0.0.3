import React from 'react';
import { StyleSheet, View, Text, Image, ScrollView, Dimensions } from 'react-native'
import { Card, ListItem, Button, Icon } from 'react-native-elements'



const ListaTexto1 = (props) =>{
    const navegarPlanBasico = () => {
        props.navigation.navigate('ListaTexto2');       
    }


    return (       
        <ScrollView style={styles.container}>
            <View>
                <Card containerStyle={styles.card}>
                    <Card.Title style={{ backgroundColor: 'orange', color: 'white'}}>PLAN BASICO</Card.Title>
                    <Card.Divider/>
                    <Card.Image source={require('../assets/card1.jpg')}>
                        <Text style={{marginBottom: 1, backgroundColor: 'orange', color: 'white'}}>
                        Explora las posibilidades de OYE LEARNING.
                        </Text>
                        <Button                        
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor:'darkorange'}}
                        title='COMENZAR' onPress={()=> navegarPlanBasico()}/>
                    </Card.Image>
                </Card>
            </View>
            <View>
                <Card containerStyle={styles.cardPremium}>
                    <Card.Title>PLAN PREMIUM</Card.Title>
                    <Button                        
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor:'lightgrey'}}
                        title='PAGA CABRON' />
                </Card>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        padding: 35,
        backgroundColor: 'white'
    },
    card: {
        flex:1,
        padding: 35,
        backgroundColor: 'orange',
        color: 'white',
        borderWidth: 1,
        borderColor: '#cccccc',
        height: Dimensions.get('window').height/2
    },
    cardPremium:{  
        flex:0.1,
        padding: 35,      
        backgroundColor: 'grey',
        color: 'white',
        borderWidth: 1,
        borderColor: '#cccccc',
        height: Dimensions.get('window').height/6
    }
  });

export default ListaTexto1;