import React from 'react';
import { StyleSheet, View, Text, Image, ScrollView, Dimensions } from 'react-native'
import { Card, ListItem, Button, Icon } from 'react-native-elements'



const ListaTexto2 = (props) =>{

    const navegarJuegos = () => {
        props.navigation.navigate('ListaTexto3');       
    }

    return (  
        <ScrollView>    
            <View style={styles.container}>
                <View style={styles.item}>
                    <Card>                
                        <Card.Image source={require('../assets/card1.jpg')}>                    
                        </Card.Image>
                    </Card>
                </View>
                <View style={styles.item}>
                    <Card>                
                        <Card.Image source={require('../assets/card1.jpg')}>
                        
                        </Card.Image>
                    </Card>
                </View>
            </View>
            <View>
                <Card>
                    <Card.Title style={{ backgroundColor: 'orange', color: 'white'}}>LOOP FONOLOGICO</Card.Title>
                    <Button                        
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor:'darkorange'}}
                        title='FONOLOGICO' onPress={()=> navegarJuegos()}/>                  
                </Card>
            </View>
            <View>
                <Card>
                    <Card.Title style={{ backgroundColor: 'orange', color: 'white'}}>AGENDA VISIOESPACIAL</Card.Title>
                    <Button                        
                        buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, backgroundColor:'darkorange'}}
                        title='VISIOESPACIAL' onPress={()=> navegarJuegos()}/>
                    
                </Card>
            </View>
        </ScrollView> 
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
      },
      item: {
        width: '50%' // is 50% of container width
      }
});

export default ListaTexto2;