import React , { useState } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, Dimensions } from 'react-native'
import { Card, ListItem, Avatar, Button, Icon, Tile,Overlay  } from 'react-native-elements'
import Modal from 'modal-react-native-web';

const list = [
    {
      name: 'Amy Farha',
      avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
      subtitle: 'Vice President'
    },
    {
      name: 'Chris Jackson',
      avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
      subtitle: 'Vice Chairman'
    },
    
  ]

const OverlayExample = () => {
    const [visible, setVisible] = useState(false);
  
    const toggleOverlay = () => {
      setVisible(!visible);
    };
  
    return (
      <View>
        <Button title="Open Overlay" onPress={toggleOverlay} />
  
        <Overlay  ModalComponent={Modal} isVisible={visible} onBackdropPress={toggleOverlay}>
          <Text>Hello from Overlay!</Text>
        </Overlay>
      </View>
    );
  };


const ListaTexto5 = () =>{
    return (
        <ScrollView>  
            <View style={styles.container}>
                <Text>Evaluacion. Criterios:</Text>
                <View>
                {
                    list.map((l, i) => (
                    <ListItem key={i} bottomDivider>
                        <Avatar source={{uri: l.avatar_url}} />
                        <ListItem.Content>
                        <ListItem.Title>{l.name}</ListItem.Title>
                        <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
                        </ListItem.Content>
                    </ListItem>
                    ))
                }
                </View>   
            </View>  
            <View>
                <OverlayExample/>    
            </View>          
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'col',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
      },
      explicacion: {
        width: '40%' // is 50% of container width
      },
      tips: {
        width: '20%' // is 50% of container width
      },
      video: {
        width: '40%' // is 50% of container width
      }
});
export default ListaTexto5;