import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

import ListaTexto1 from "./screens/Pantalla1";
import ListaTexto2 from "./screens/Pantalla2";
import ListaTexto3 from "./screens/Pantalla3";
import ListaTexto4 from "./screens/Pantalla4";
import ListaTexto5 from "./screens/Pantalla5";

function MyStack() {
  return(
    <Stack.Navigator>
      <Stack.Screen name="ListaTexto1" component={ListaTexto1} options={{title: 'Pantalla principal'}}/>
      <Stack.Screen name="ListaTexto2" component={ListaTexto2} options={{title: 'Oye Learning'}}/>
      <Stack.Screen name="ListaTexto3" component={ListaTexto3} options={{title: 'Oye Learning'}}/>
      <Stack.Screen name="ListaTexto4" component={ListaTexto4} options={{title: 'Oye Learning'}}/>
      <Stack.Screen name="ListaTexto5" component={ListaTexto5} options={{title: 'Oye Learning'}}/>
    </Stack.Navigator>
  )  
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack/>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
